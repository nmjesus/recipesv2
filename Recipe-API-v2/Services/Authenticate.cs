using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Recipe_API_v2.Helpers;
using Recipe_API_v2.Models;
using System.Linq;

namespace Recipe_API_v2.Services {
  public interface IAuthenticateService {
      AuthorBaseDTO Authenticate(string email, string password);
  }

  public class AuthenticateService : IAuthenticateService {
    private readonly AppSettings _appSettings;
    private readonly IUnitOfWork _unitOfWork;

    public AuthenticateService(IOptions<AppSettings> appSettings, IUnitOfWork unitOfWork) {
      _appSettings = appSettings.Value;
      _unitOfWork = unitOfWork;
    }

    public AuthorBaseDTO Authenticate(string email, string password) {
      var author = _unitOfWork.AuthorRepository.Login(email, password);

      if (author == null) {
        return null;
      }

      var tokenHandler = new JwtSecurityTokenHandler();
      var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
      var tokenDescriptor = new SecurityTokenDescriptor {
          Subject = new ClaimsIdentity(new Claim[] {
            new Claim(ClaimTypes.Email, author.Email),
            new Claim(ClaimTypes.GivenName, author.FirstName),
            new Claim(ClaimTypes.Surname, author.LastName)
          }),
          Expires = DateTime.UtcNow.AddDays(7),
          SigningCredentials = new SigningCredentials(
            new SymmetricSecurityKey(key),
            SecurityAlgorithms.HmacSha256Signature
          )
      };
      var token = tokenHandler.CreateToken(tokenDescriptor);

      return new AuthorBaseDTO {
        Id = author.Id,
        FirstName = author.FirstName,
        LastName = author.LastName,
        Email = author.Email,
        Token = tokenHandler.WriteToken(token),
      };
    }
  }
}