﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RecipeAPIv2.Migrations
{
    public partial class Indexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Recipes_PublicId",
                table: "Recipes",
                column: "PublicId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Authors_PublicId",
                table: "Authors",
                column: "PublicId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Recipes_PublicId",
                table: "Recipes");

            migrationBuilder.DropIndex(
                name: "IX_Authors_PublicId",
                table: "Authors");
        }
    }
}
