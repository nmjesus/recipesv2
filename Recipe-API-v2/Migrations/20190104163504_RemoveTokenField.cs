﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RecipeAPIv2.Migrations
{
    public partial class RemoveTokenField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Token",
                table: "Authors");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Token",
                table: "Authors",
                nullable: true);
        }
    }
}
