using Recipe_API_v2.Models;
using Recipe_API_v2.Repositories;

namespace Recipe_API_v2 {
  public interface IUnitOfWork {
    IAuthorRepository AuthorRepository { get; }
    IRecipeRepository RecipeRepository { get; }
    IRepository<IngredientPerRecipe> IngredientPerRecipeRepository { get; }

    void Commit();
    void RejectChanges();
    void Dispose();
  }
}