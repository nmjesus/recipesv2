using Microsoft.EntityFrameworkCore;
using Recipe_API_v2.Models;
using Recipe_API_v2.Repositories;
using System.Linq;

namespace Recipe_API_v2
{
  public class UnitOfWork: IUnitOfWork {
    public readonly RecipeAPIContext _context;
    public IAuthorRepository AuthorRepository => new AuthorRepository(_context);
    public IRecipeRepository RecipeRepository => new RecipeRepository(_context);
    public IRepository<IngredientPerRecipe> IngredientPerRecipeRepository => new Repository<IngredientPerRecipe>(_context);

    public UnitOfWork(RecipeAPIContext context) {
      _context = context;
    }

    public void Commit() {
      _context.SaveChanges();
    }

    public void Dispose() {
      _context.Dispose();
    }

    public void RejectChanges() {
      foreach (var entry in _context.ChangeTracker.Entries().Where(e => e.State != EntityState.Unchanged)) {
        switch (entry.State) {
          case EntityState.Added:
            entry.State = EntityState.Detached;
            break;
          case EntityState.Modified:
          case EntityState.Deleted:
            entry.Reload();
            break;
        }
      }
    }
  }
}