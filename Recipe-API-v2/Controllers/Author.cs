using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using Recipe_API_v2.Models;
using Microsoft.AspNetCore.Authorization;
using Recipe_API_v2.Services;

namespace Recipe_API_v2.Controllers {
  [Authorize]
  [Route("api/author")]
  [ApiController]
  public class Authors: ControllerBase {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAuthenticateService _authenticateService;

    public Authors(IUnitOfWork unitOfWork, IAuthenticateService authenticateService) {
      _unitOfWork = unitOfWork;
      _authenticateService = authenticateService;
    }

    [AllowAnonymous]
    [HttpPost("login")]
    public IActionResult Login(AuthorPayloadDTO author) {
      var authenticatedAuthor = _authenticateService.Authenticate(author.Email, author.Password);
      if (authenticatedAuthor == null) {
        return BadRequest(new { message = "Username or password is incorrect" });
      }
      return Ok(new AuthorBaseDTO {
        Id = authenticatedAuthor.Id,
        FirstName = authenticatedAuthor.FirstName,
        LastName = authenticatedAuthor.LastName,
        Token = authenticatedAuthor.Token,
        Email = authenticatedAuthor.Email,
      });
    }

    [HttpGet]
    public IEnumerable<AuthorRecipesDTO> Index() {
      return _unitOfWork.AuthorRepository.Get();
    }

    [HttpGet]
    [Route("{id}")]
    public ActionResult<AuthorRecipesDTO> Get(Guid id) {
      var item = _unitOfWork.AuthorRepository.Get(id);
      if (item == null) {
        return NotFound();
      }
      return item;
    }

    [AllowAnonymous]
    [HttpPost]
    public NoContentResult Add(AuthorPayloadDTO author) {
      _unitOfWork.AuthorRepository.Add(new Author {
        FirstName = author.FirstName,
        LastName = author.LastName,
        Password = author.Password,
        Email = author.Email,
        PublicId = Guid.NewGuid()
      });
      _unitOfWork.Commit();
      return NoContent();
    }
  }
}