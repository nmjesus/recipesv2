using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using Recipe_API_v2.Models;

namespace Recipe_API_v2.Controllers {
  [Route("api/recipe")]
  [ApiController]
  public class Recipes: ControllerBase {
    private readonly IUnitOfWork _unitOfWork;

    public Recipes(IUnitOfWork unitOfWork) {
      _unitOfWork = unitOfWork;
    }

    [HttpGet]
    public IEnumerable<RecipeDTO> Index() {
      return _unitOfWork.RecipeRepository.Get();
    }

    [HttpGet]
    [Route("{id}")]
    public ActionResult<RecipeDTO> Get(Guid id) {
      var item = _unitOfWork.RecipeRepository.Get(id);
      if (item == null) {
        return NotFound();
      }
      return item;
    }

    [HttpPost]
    public NoContentResult Add(RecipePayloadDTO recipe) {
      var newRecipe = _unitOfWork.RecipeRepository.Add(new Recipe {
        PublicId = Guid.NewGuid(),
        Title = recipe.Title,
        Description = recipe.Description,
        PreparationTime = recipe.PreparationTime,
        AuthorId = recipe.Author,
      }, 0);
      _unitOfWork.Commit();

      foreach (var ingredient in recipe.Ingredients) {
        _unitOfWork.IngredientPerRecipeRepository.Add(new IngredientPerRecipe {
          PublicId = Guid.NewGuid(),
          RecipeId = newRecipe.Entity.Id,
          IngredientId = ingredient.Ingredient,
          UnitId = ingredient.Unit,
          Quantity = ingredient.Quantity,
        });
      }
      _unitOfWork.Commit();
      return NoContent();
    }

    [HttpPut]
    [Route("{id}")]
    public NoContentResult Update(Recipe recipe) {
      _unitOfWork.RecipeRepository.Update(new Recipe {
        Title = recipe.Title,
        Description = recipe.Description,
        PreparationTime = recipe.PreparationTime,
      });
      _unitOfWork.Commit();
      return NoContent();
    }
  }
}