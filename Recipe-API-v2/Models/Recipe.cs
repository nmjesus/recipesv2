using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Recipe_API_v2.Models {
  public interface IRecipe {
    string Title { get; set; }
    string Description { get; set; }
    int PreparationTime { get; set; }
  }

  [Table("Recipes")]
  public class Recipe: Entity, IRecipe {
    public string Title { get; set; }
    public string Description { get; set; }
    public int PreparationTime { get; set; }

    public int AuthorId { get; set; }
    public virtual Author Author { get; set; }
  }

  public class RecipeBaseDTO : IRecipe {
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public int PreparationTime { get; set; }
  }

  public class RecipeDTO: RecipeBaseDTO {
    public virtual AuthorBaseDTO Author { get; set; }
    public virtual IEnumerable<IngredientPerRecipeDTO> Ingredients { get; set; }
  }

  public class RecipePayloadDTO: RecipeBaseDTO {
    public int RecipeId { get; set; }
    public virtual int Author { get; set; }
    public virtual IEnumerable<IngredientPayloadDTO> Ingredients { get; set; }
  }
}