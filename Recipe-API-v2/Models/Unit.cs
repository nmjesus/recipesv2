using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Recipe_API_v2.Models {
  public interface IUnit {
    string Name { get; set; }
  }
  
  public class Unit: Entity, IUnit {
    public string Name { get; set; }
  }

  public class UnitDTO: IUnit {
    public string Name { get; set; }
  }
}