using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Recipe_API_v2.Models {
  public interface IAuthor {
    string FirstName { get; set; }
    string LastName { get; set; }
  }

  [Table("Authors")]
  public class Author: Entity, IAuthor {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public virtual IEnumerable<Recipe> Recipes { get; set; }
  }

  public class AuthorBaseDTO: IAuthor {
    public Guid Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Token { get; set; }
  }

  public class AuthorRecipesDTO: AuthorBaseDTO {
    public virtual IEnumerable<RecipeBaseDTO> Recipes { get; set; }
  }

  public class AuthorPayloadDTO: AuthorBaseDTO {
    public string Password { get; set; }
  }
}