using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Recipe_API_v2.Models {
  public interface IIngredientPerRecipe {
    float Quantity { get; set; }
  }
  
  // TODO: Remove PublicId from this table
  public class IngredientPerRecipe: Entity, IIngredientPerRecipe {
    public float Quantity { get; set; }

    public int RecipeId { get; set; }
    public Recipe Recipe { get; set; }

    public int IngredientId { get; set; }
    public Ingredient Ingredient { get; set; }

    public int UnitId { get; set; }
    public Unit Unit { get; set; }
  }

  public class IngredientPerRecipeDTO : IIngredientPerRecipe
  {
    public string Unit { get; set; }
    public string Ingredient { get; set; }
    public float Quantity { get; set; }
  }
}