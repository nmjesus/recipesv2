using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Recipe_API_v2.Models {
  public interface IIngredient {
    string Name { get; set; }
  }
  
  public class Ingredient: Entity, IIngredient {
    public string Name { get; set; }
  }

  public class IngredientDTO: IIngredient {
    public string Name { get; set; }
  }

  public class IngredientPayloadDTO {
    public int Unit { get; set; }
    public int Ingredient { get; set; }
    public int Quantity { get; set; }
  }
}