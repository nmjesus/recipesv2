using System;

namespace Recipe_API_v2.Models {
  public abstract class Entity {
    public int Id { get; set; }
    public Guid PublicId { get; set; }
  }
}