using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Recipe_API_v2.Models;

namespace Recipe_API_v2.Repositories
{
  public class Repository<T> : IRepository<T> where T : Entity {
    protected readonly RecipeAPIContext _context;

    public Repository(RecipeAPIContext context) {
      _context = context;
    }

    public void Add(T entity) {
      this._context.Set<T>().Add(entity);
    }

    public EntityEntry<T> Add(T entity, int? noop) {
      return this._context.Set<T>().Add(entity);
    }

    public void Delete(T entity) {
      this._context.Set<T>().Remove(entity);
    }

    public IEnumerable<T> Get()
    {
      return this._context.Set<T>().ToList().AsEnumerable();
    }

    public T Get(Guid id)
    {
      return this._context.Set<T>().Where(e => e.PublicId == id).FirstOrDefault();
    }

    public T Get(int id)
    {
      return this._context.Set<T>().Where(e => e.Id == id).FirstOrDefault();
    }

    public IEnumerable<T> Get(Expression<Func<T, bool>> predicate) {
      return this._context.Set<T>().Where(predicate).AsEnumerable();
    }

    public void Update(T entity)
    {
      this._context.Set<T>().Update(entity);
    }
  }
}