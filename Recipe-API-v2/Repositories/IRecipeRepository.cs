using System;
using System.Collections.Generic;
using Recipe_API_v2.Models;

namespace Recipe_API_v2.Repositories
{
  public interface IRecipeRepository: IRepository<Recipe> {
    new IEnumerable<RecipeDTO> Get();
    new RecipeDTO Get(Guid id);
  }
}