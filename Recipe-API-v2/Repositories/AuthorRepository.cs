using System;
using System.Collections.Generic;
using System.Linq;
using Recipe_API_v2.Models;

namespace Recipe_API_v2.Repositories
{
  public class AuthorRepository : Repository<Author>, IAuthorRepository
  {
    public AuthorRepository(RecipeAPIContext context) : base(context) { }

    private IEnumerable<AuthorRecipesDTO> GetAuthors(Guid? id) {
      return this._context.Set<Author>()
        .Where(a => id != null ? a.PublicId == id : true)
        .Select(a => new AuthorRecipesDTO {
        Id = a.PublicId,
        FirstName = a.FirstName,
        LastName = a.LastName,
        Recipes = this._context.Set<Recipe>().Where(r => r.AuthorId == a.Id).Select(r => new RecipeBaseDTO {
          Id = r.PublicId,
          Title = r.Title,
          Description = r.Description,
          PreparationTime = r.PreparationTime,
        })
      });
    }

    new public IEnumerable<AuthorRecipesDTO> Get() {
      return this.GetAuthors(null);
    }

    new public AuthorRecipesDTO Get(Guid id) {
      return this.GetAuthors(id).FirstOrDefault();
    }

    public AuthorBaseDTO Login(string email, string password) {
      return this._context.Set<Author>()
        .Where(a => a.Email == email && a.Password == password)
        .Select(a => new AuthorBaseDTO() {
          Id = a.PublicId,
          Email = a.Email,
          FirstName = a.FirstName,
          LastName = a.LastName,
        }).FirstOrDefault();
    }
  }
}