using System;
using System.Collections.Generic;
using System.Linq;
using Recipe_API_v2.Models;

namespace Recipe_API_v2.Repositories
{
  public class RecipeRepository : Repository<Recipe>, IRecipeRepository
  {
    public RecipeRepository(RecipeAPIContext context) : base(context) { }

    private IEnumerable<RecipeDTO> GetRecipes(Guid? id) {
      return this._context.Set<Recipe>()
        .Where(r => id != null ? r.PublicId == id : true)
        .Join(
        this._context.Set<Author>(),
        r => r.AuthorId,
        a => a.Id,
        (r, a) => new RecipeDTO {
          Id = r.PublicId,
          Title = r.Title,
          Description = r.Description,
          PreparationTime = r.PreparationTime,
          Author = new AuthorBaseDTO {
            Id = a.PublicId,
            FirstName = a.FirstName,
            LastName = a.LastName,
          },
          Ingredients = this._context.Set<IngredientPerRecipe>().Where(ir => ir.RecipeId == r.Id).Select(ir => new IngredientPerRecipeDTO {
            Ingredient = ir.Ingredient.Name,
            Unit = ir.Unit.Name,
            Quantity = ir.Quantity
          })
        }
      );
    }

    new public IEnumerable<RecipeDTO> Get() {
      return this.GetRecipes(null);
    }

    new public RecipeDTO Get(Guid id) {
      return this.GetRecipes(id).FirstOrDefault();
    }
  }
}