using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Recipe_API_v2.Models;

namespace Recipe_API_v2.Repositories
{
  public interface IRepository<T> where T: Entity {
    IEnumerable<T> Get();
    T Get(Guid id);
    T Get(int id);
    void Add(T entity);
    EntityEntry<T> Add(T entity, int? noop);
    void Delete(T entity);
    void Update(T entity);
  }
}