using System;
using System.Collections.Generic;
using Recipe_API_v2.Models;

namespace Recipe_API_v2.Repositories
{
  public interface IAuthorRepository: IRepository<Author> {
    new IEnumerable<AuthorRecipesDTO> Get();
    new AuthorRecipesDTO Get(Guid id);
    AuthorBaseDTO Login(string email, string password);
  }
}