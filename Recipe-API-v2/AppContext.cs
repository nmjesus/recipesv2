using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Recipe_API_v2.Models;

namespace Recipe_API_v2 {
  public class RecipeAPIContext : DbContext {
    public DbSet<Author> Authors { get; set; }
    public DbSet<Recipe> Recipes { get; set; }
    public DbSet<IngredientPerRecipe> IngredientPerRecipes { get; set; }
    private bool _configured { get; set; }

    private readonly string ConnectionString = "Host=127.0.0.1;Port=5432;Database=RecipeAPIV2;Username=postgres";

    public static readonly LoggerFactory Logger = new LoggerFactory(new[] {
      new ConsoleLoggerProvider((_, __) => true, true)
    });

    public RecipeAPIContext() : base() {}
    public RecipeAPIContext(DbContextOptions options) : base(options) {
      this._configured = true;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
      if (!this._configured) {
        optionsBuilder.UseNpgsql(ConnectionString);
        optionsBuilder.UseLoggerFactory(Logger);
        this._configured = true;
      }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder) {
      modelBuilder.Entity<Author>().HasIndex(a => a.PublicId).IsUnique();
      modelBuilder.Entity<Recipe>().HasIndex(r => r.PublicId).IsUnique();
    }
  }
}