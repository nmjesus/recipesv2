using System;
using Xunit;
using Moq;
using Recipe_API_v2.Models;
using System.Linq;
using System.Collections.Generic;

namespace Recipe_API_v2.Tests
{
  public class AuthorRepository {
    [Fact]
    public void AddAuthor() {
      // setup
      var unitOfWork = new Setup().unitOfWork;
      var uuid = Guid.NewGuid();

      // arrange
      var author = new Author {
        FirstName = "Nuno",
        LastName = "Jesus",
        PublicId = uuid,
      };
      unitOfWork.AuthorRepository.Add(author);
      unitOfWork.Commit();

      // assert
      Assert.Single(unitOfWork.AuthorRepository.Get());
      Assert.Empty(unitOfWork.AuthorRepository.Get(uuid).Recipes);
    }

    [Fact]
    public void ListSingleAuthorWithRecipe() {
      // setup
      var unitOfWork = new Setup().unitOfWork;
      var uuid = Guid.NewGuid();

      // arrange
      var author = new Author {
        FirstName = "Nuno",
        LastName = "Jesus",
        PublicId = uuid,
      };
      unitOfWork.AuthorRepository.Add(author);
      unitOfWork.Commit();

      var recipe = new Recipe {
        Title = "Huevos rotos",
        Description = "Huevos rotos con sal",
        PreparationTime = 5,
        PublicId = Guid.NewGuid(),
        AuthorId = 1,
      };
      unitOfWork.RecipeRepository.Add(recipe);
      unitOfWork.Commit();

      // assert
      Assert.Single(unitOfWork.AuthorRepository.Get());
      Assert.Single(unitOfWork.AuthorRepository.Get(uuid).Recipes);
      Assert.IsType<AuthorRecipesDTO>(unitOfWork.AuthorRepository.Get(uuid));
      Assert.IsType<List<RecipeBaseDTO>>(unitOfWork.AuthorRepository.Get(uuid).Recipes.ToList());
    }

    [Fact]
    public void ListAuthorsWithRecipe() {
      // setup
      var unitOfWork = new Setup().unitOfWork;
      var uuid1 = Guid.NewGuid();
      var uuid2 = Guid.NewGuid();

      // arrange
      var author1 = new Author {
        FirstName = "Nuno",
        LastName = "Jesus",
        PublicId = uuid1,
      };
      var author2 = new Author {
        FirstName = "Nuno",
        LastName = "Miguel",
        PublicId = uuid2,
      };
      unitOfWork.AuthorRepository.Add(author1);
      unitOfWork.AuthorRepository.Add(author2);
      unitOfWork.Commit();

      var recipe = new Recipe {
        Title = "Huevos rotos",
        Description = "Huevos rotos con sal",
        PreparationTime = 5,
        PublicId = Guid.NewGuid(),
        AuthorId = 1,
      };
      unitOfWork.RecipeRepository.Add(recipe);
      unitOfWork.Commit();

      // assert
      Assert.True(unitOfWork.AuthorRepository.Get().Count() == 2);
      Assert.Single(unitOfWork.AuthorRepository.Get(uuid1).Recipes);
      Assert.IsType<List<AuthorRecipesDTO>>(unitOfWork.AuthorRepository.Get().ToList());
      Assert.IsType<List<RecipeBaseDTO>>(unitOfWork.AuthorRepository.Get().First().Recipes.ToList());
    }
  }
}



// SAMPLE MOQ
// [Fact]
// public void Test1() {
//   var unitOfWorkMock = new Mock<IUnitOfWork>();
//   var authorRepositoryMock = new Mock<IAuthorRepository>();
//   authorRepositoryMock.Setup(a => a.Get()).Returns(new List<AuthorRecipesDTO> {
//     new AuthorRecipesDTO {
//       Id = Guid.NewGuid(),
//       FirstName = "Nuno",
//       LastName = "Jesus",
//       Recipes = new List<RecipeBaseDTO>()
//     }
//   });
//   unitOfWorkMock.Setup(u => u.AuthorRepository).Returns(authorRepositoryMock.Object);
//   var author = new Author {
//     FirstName = "Nuno",
//     LastName = "Jesus",
//     PublicId = Guid.NewGuid()
//   };
//   // authorRepositoryMock.Setup(repo => repo.Add(author));
//   unitOfWorkMock.Object.AuthorRepository.Add(author);
//   unitOfWorkMock.Object.Commit();
//   Assert.True(unitOfWorkMock.Object.AuthorRepository.Get().Count() == 1, "Should add one author");
// }