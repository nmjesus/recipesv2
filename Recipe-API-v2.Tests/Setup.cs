using System;
using Microsoft.EntityFrameworkCore;

namespace Recipe_API_v2.Tests {
  public class Setup {
    public UnitOfWork unitOfWork { get; }
    public RecipeAPIContext ctx { get; }

    public Setup() {
      DbContextOptionsBuilder optionsBuilder = new DbContextOptionsBuilder<RecipeAPIContext>();
      optionsBuilder.UseInMemoryDatabase(Guid.NewGuid().ToString());

      this.ctx = new RecipeAPIContext(optionsBuilder.Options);
      this.unitOfWork = new UnitOfWork(this.ctx);
    }
  }
}